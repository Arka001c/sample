QA Dashboard Readme File
---
- Download the STS Application (latest preferable)
[Windows (STS4) download link](https://download.springsource.com/release/STS4/4.6.1.RELEASE/dist/e4.15/spring-tool-suite-4-4.6.1.RELEASE-e4.15.0-win32.win32.x86_64.self-extracting.jar )
- [Download JDK](https://www.oracle.com/java/technologies/javase/jdk13-archive-downloads.html)

Git Commands
---
**Clone Existing Repository**
- git clone https://gitlab.com/bhaskarmhnt/qa_dashboard_spring.git

**Push an existing folder**
- cd existing_folder
- git init
- git add .
- git commit -m "Give a note for this commit"
- git push -u https://gitlab.com/bhaskarmhnt/qa_dashboard_spring.git master


**Pull from Existing Repository**
- cd existing_folder
- git pull https://gitlab.com/bhaskarmhnt/qa_dashboard_spring.git master

Import Spring Project to STS
---
- Go to `File` then `Open Projects from file system..`
- Go to the project directory and select the project and open. (Wait until the build process completes).
- Select the project from project explorer.
- `Right Click` on it and go to Run as `Spring Boot App`

>Once you followed all these steps you got your Server running

Go to the Dashboard
---
- Open `Browser` and [click here...](http://localhost:8080/qadashboard/index.html)

- Email Id `bhaskar.mohanta@simpsoftsolutions.com`
- Password `user`
> If http://localhost:8080/ is not accessible from your machine then check the port to the console of `spring boot`