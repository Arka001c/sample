package com.simpsoft.qadashboard.Models;

import java.util.List;

public class CustomViewConfigModel {

	private int View_Configuration_ID;
	private String View_Configuration_Name;
	private int Owner;
	private String Filter_Field_Value;
	private int filter_Field_ID;
	private int Filter_Field;
	private List<String> drillDownList;
	//Add new field to your need
	public int getView_Configuration_ID() {
		return View_Configuration_ID;
	}
	public void setView_Configuration_ID(int view_Configuration_ID) {
		View_Configuration_ID = view_Configuration_ID;
	}
	public String getView_Configuration_Name() {
		return View_Configuration_Name;
	}
	public void setView_Configuration_Name(String view_Configuration_Name) {
		View_Configuration_Name = view_Configuration_Name;
	}
	public int getOwner() {
		return Owner;
	}
	public void setOwner(int owner) {
		Owner = owner;
	}
	public String getFilter_Field_Value() {
		return Filter_Field_Value;
	}
	public void setFilter_Field_Value(String filter_Field_Value) {
		Filter_Field_Value = filter_Field_Value;
	}
	public int getFilter_Field_ID() {
		return filter_Field_ID;
	}
	public void setFilter_Field_ID(int filter_Field_ID) {
		this.filter_Field_ID = filter_Field_ID;
	}
	public int getFilter_Field() {
		return Filter_Field;
	}
	public void setFilter_Field(int filter_Field) {
		Filter_Field = filter_Field;
	}
	public List<String> getDrillDownList() {
		return drillDownList;
	}
	public void setDrillDownList(List<String> drillDownList) {
		this.drillDownList = drillDownList;
	}
	@Override
	public String toString() {
		return "CustomViewConfigModel [View_Configuration_ID=" + View_Configuration_ID + ", View_Configuration_Name="
				+ View_Configuration_Name + ", Owner=" + Owner + ", Filter_Field_Value=" + Filter_Field_Value
				+ ", filter_Field_ID=" + filter_Field_ID + ", Filter_Field=" + Filter_Field + ", drillDownList="
				+ drillDownList + "]";
	}
}
