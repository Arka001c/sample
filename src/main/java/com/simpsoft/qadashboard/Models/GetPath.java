package com.simpsoft.qadashboard.Models;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class GetPath {
	@Value("${output_directory}")
	private String getPath;
	
	@Value("${XMLPath}")
	private String XMLPath;
	
	public String getGetPath() {
		return getPath;
	}
	public void setGetPath(String getPath) {
		this.getPath = getPath;
	}
	public String getXMLPath() {
		return XMLPath;
	}
	public void setXMLPath(String xMLPath) {
		XMLPath = xMLPath;
	}
}
