package com.simpsoft.qadashboard.Models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="view_configuration")
public class ViewConfigurationModel {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int View_Configuration_ID;
	private String View_Configuration_Name;
	private int Private_Flag;
	private int Owner;
	
	public int getView_Configuration_ID() {
		return View_Configuration_ID;
	}
	public void setView_Configuration_ID(int view_Configuration_ID) {
		View_Configuration_ID = view_Configuration_ID;
	}
	public String getView_Configuration_Name() {
		return View_Configuration_Name;
	}
	public void setView_Configuration_Name(String view_Configuration_Name) {
		View_Configuration_Name = view_Configuration_Name;
	}
	public int getPrivate_Flag() {
		return Private_Flag;
	}
	public void setPrivate_Flag(int private_Flag) {
		Private_Flag = private_Flag;
	}
	public int getOwner() {
		return Owner;
	}
	public void setOwner(int owner) {
		Owner = owner;
	}
	
}
