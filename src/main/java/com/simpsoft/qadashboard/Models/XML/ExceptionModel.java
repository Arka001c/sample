package com.simpsoft.qadashboard.Models.XML;

import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class ExceptionModel {
	private String message;
	private List<FrameModel> frame;

	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<FrameModel> getFrame() {
		return frame;
	}
	public void setFrame(List<FrameModel> frame) {
		this.frame = frame;
	}
	@Override
	public String toString() {
		return "ExceptionModel [message=" + message + ", frame=" + frame + "]";
	}
}
