package com.simpsoft.qadashboard.Models.XML;

import org.springframework.stereotype.Component;

@Component
public class FrameModel {
	private String classString;
	private String method;
	private String line;
	
	public String getClassString() {
		return classString;
	}
	public void setClassString(String classString) {
		this.classString = classString;
	}
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	public String getLine() {
		return line;
	}
	public void setLine(String line) {
		this.line = line;
	}
	@Override
	public String toString() {
		return "FrameModel [classString=" + classString + ", method=" + method + ", line=" + line + "]";
	}
}
