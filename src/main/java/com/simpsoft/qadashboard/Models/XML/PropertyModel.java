package com.simpsoft.qadashboard.Models.XML;

import org.springframework.stereotype.Component;

@Component
public class PropertyModel {
	private String name;
	private String value;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	@Override
	public String toString() {
		return "PropertyModel [name=" + name + ", value=" + value + "]";
	}
	
}
