package com.simpsoft.qadashboard.Models;

import org.springframework.stereotype.Component;

@Component
public class JiraCountByStatusModel {
	private int jira_count;
	private String jira_status;
	
	public int getJira_count() {
		return jira_count;
	}
	public void setJira_count(int jira_count) {
		this.jira_count = jira_count;
	}
	public String getJira_status() {
		return jira_status;
	}
	public void setJira_status(String jira_status) {
		this.jira_status = jira_status;
	}
}
