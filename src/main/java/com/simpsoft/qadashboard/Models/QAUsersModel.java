package com.simpsoft.qadashboard.Models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="qa_users")
public class QAUsersModel {

	@Id
	private int User_ID;
	private String User_Name;
	private String Email;
	private int Active_Flag;
	
	public int getUser_ID() {
		return User_ID;
	}
	public void setUser_ID(int user_ID) {
		User_ID = user_ID;
	}
	public String getUser_Name() {
		return User_Name;
	}
	public void setUser_Name(String user_Name) {
		User_Name = user_Name;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
	public int getActive_Flag() {
		return Active_Flag;
	}
	public void setActive_Flag(int active_Flag) {
		Active_Flag = active_Flag;
	}
	
	
}
