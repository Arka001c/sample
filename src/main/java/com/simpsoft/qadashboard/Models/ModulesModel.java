package com.simpsoft.qadashboard.Models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "modules")
public class ModulesModel {

	@Id
	private int Module_ID;
	private int Project_ID;
	private String Module_Name;
	
	public int getModule_ID() {
		return Module_ID;
	}
	public void setModule_ID(int module_ID) {
		Module_ID = module_ID;
	}
	public int getProject_ID() {
		return Project_ID;
	}
	public void setProject_ID(int project_ID) {
		Project_ID = project_ID;
	}
	public String getModule_Name() {
		return Module_Name;
	}
	public void setModule_Name(String module_Name) {
		Module_Name = module_Name;
	}
	@Override
	public String toString() {
		return "ModulesModel [Module_ID=" + Module_ID + ", Project_ID=" + Project_ID + ", Module_Name=" + Module_Name
				+ "]";
	}
	
}
