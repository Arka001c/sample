package com.simpsoft.qadashboard.Models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="release_run_association")
public class ReleaseRunAssociationModel {

	@Id
	private int Release_Run_Association_ID;
	private int Release_ID;
	private int Run_Definition_ID;
	
	public int getRelease_Run_Association_ID() {
		return Release_Run_Association_ID;
	}
	public void setRelease_Run_Association_ID(int release_Run_Association_ID) {
		Release_Run_Association_ID = release_Run_Association_ID;
	}
	public int getRelease_ID() {
		return Release_ID;
	}
	public void setRelease_ID(int release_ID) {
		Release_ID = release_ID;
	}
	public int getRun_Definition_ID() {
		return Run_Definition_ID;
	}
	public void setRun_Definition_ID(int run_Definition_ID) {
		Run_Definition_ID = run_Definition_ID;
	}
	
}
