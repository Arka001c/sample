package com.simpsoft.qadashboard.Models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="entity_fields")
public class EntityFieldsModel {

	@Id
	private int Entity_Fields_ID;
	private String Entity_Fields_Name;
	public int getEntity_Fields_ID() {
		return Entity_Fields_ID;
	}
	public void setEntity_Fields_ID(int entity_Fields_ID) {
		Entity_Fields_ID = entity_Fields_ID;
	}
	public String getEntity_Fields_Name() {
		return Entity_Fields_Name;
	}
	public void setEntity_Fields_Name(String entity_Fields_Name) {
		Entity_Fields_Name = entity_Fields_Name;
	}
	
}
