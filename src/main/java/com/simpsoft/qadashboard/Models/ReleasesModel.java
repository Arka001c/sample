package com.simpsoft.qadashboard.Models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="releases")
public class ReleasesModel {

	@Id
	private int Release_ID;
	private String Release_Name;
	private Date Start_Date;
	private Date End_Date;
	
	public int getRelease_ID() {
		return Release_ID;
	}
	public void setRelease_ID(int release_ID) {
		Release_ID = release_ID;
	}
	public String getRelease_Name() {
		return Release_Name;
	}
	public void setRelease_Name(String release_Name) {
		Release_Name = release_Name;
	}
	public Date getStart_Date() {
		return Start_Date;
	}
	public void setStart_Date(Date start_Date) {
		Start_Date = start_Date;
	}
	public Date getEnd_Date() {
		return End_Date;
	}
	public void setEnd_Date(Date end_Date) {
		End_Date = end_Date;
	}
	
}
