package com.simpsoft.qadashboard.Models;

import org.springframework.stereotype.Component;

@Component
public class CustomFilterReturnModel {

	private String data_Name;
	private String data_id;
	private String data_type;
	
	public String getData_Name() {
		return data_Name;
	}
	public void setData_Name(String data_Name) {
		this.data_Name = data_Name;
	}
	public String getData_id() {
		return data_id;
	}
	public void setData_id(String data_id) {
		this.data_id = data_id;
	}
	public String getData_type() {
		return data_type;
	}
	public void setData_type(String data_type) {
		this.data_type = data_type;
	}	
}
