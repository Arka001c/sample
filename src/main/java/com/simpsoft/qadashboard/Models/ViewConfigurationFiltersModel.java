package com.simpsoft.qadashboard.Models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="view_configuration_filters")
public class ViewConfigurationFiltersModel {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int View_Configuration_Filters_ID;
	private String Filter_Field_Value;
	private int View_Configuration_ID;
	private int Filter_Field;
	private int Filter_Field_ID;
	
	public int getView_Configuration_Filters_ID() {
		return View_Configuration_Filters_ID;
	}
	public void setView_Configuration_Filters_ID(int view_Configuration_Filters_ID) {
		View_Configuration_Filters_ID = view_Configuration_Filters_ID;
	}
	public String getFilter_Field_Value() {
		return Filter_Field_Value;
	}
	public void setFilter_Field_Value(String filter_Field_Value) {
		Filter_Field_Value = filter_Field_Value;
	}
	public int getView_Configuration_ID() {
		return View_Configuration_ID;
	}
	public void setView_Configuration_ID(int view_Configuration_ID) {
		View_Configuration_ID = view_Configuration_ID;
	}
	public int getFilter_Field() {
		return Filter_Field;
	}
	public void setFilter_Field(int filter_Field) {
		Filter_Field = filter_Field;
	}
	public int getFilter_Field_ID() {
		return Filter_Field_ID;
	}
	public void setFilter_Field_ID(int filter_Field_ID) {
		Filter_Field_ID = filter_Field_ID;
	}
	
}
