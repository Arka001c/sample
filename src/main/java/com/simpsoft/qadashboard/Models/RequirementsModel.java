package com.simpsoft.qadashboard.Models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "requirements")
public class RequirementsModel {

	@Id
	private int Requirement_ID;
	private String Requirement_Desc;
	private int Release_ID;
	private int Project_ID;
	
	public int getRequirement_ID() {
		return Requirement_ID;
	}
	public void setRequirement_ID(int requirement_ID) {
		Requirement_ID = requirement_ID;
	}
	public String getRequirement_Desc() {
		return Requirement_Desc;
	}
	public void setRequirement_Desc(String requirement_Desc) {
		Requirement_Desc = requirement_Desc;
	}
	public int getRelease_ID() {
		return Release_ID;
	}
	public void setRelease_ID(int release_ID) {
		Release_ID = release_ID;
	}
	public int getProject_ID() {
		return Project_ID;
	}
	public void setProject_ID(int project_ID) {
		Project_ID = project_ID;
	}
}
