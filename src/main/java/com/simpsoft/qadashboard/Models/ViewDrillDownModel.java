package com.simpsoft.qadashboard.Models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="view_drill_down")
public class ViewDrillDownModel {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int View_Drill_Down_ID;
	private int Drill_Down_Sl;
	private int View_Configuration_ID;
	private int Field_ID;
	
	public int getView_Drill_Down_ID() {
		return View_Drill_Down_ID;
	}
	public void setView_Drill_Down_ID(int view_Drill_Down_ID) {
		View_Drill_Down_ID = view_Drill_Down_ID;
	}
	public int getDrill_Down_Sl() {
		return Drill_Down_Sl;
	}
	public void setDrill_Down_Sl(int drill_Down_Sl) {
		Drill_Down_Sl = drill_Down_Sl;
	}
	public int getView_Configuration_ID() {
		return View_Configuration_ID;
	}
	public void setView_Configuration_ID(int view_Configuration_ID) {
		View_Configuration_ID = view_Configuration_ID;
	}
	public int getField_ID() {
		return Field_ID;
	}
	public void setField_ID(int field_ID) {
		Field_ID = field_ID;
	}
	
}
