package com.simpsoft.qadashboard.Models;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class JiraStateReturnModel {

	@Id
	private int JIRA_ID;
	private String JIRA_Desc;
	private int Requirement_ID;
	private int Functional_Owner_User_ID;
	private int Project_ID;
	private int Release_ID;
	
	private Date Start_Date;
	private Date End_Date;
	private Date State_Modified;
	private int ModifiedBy_User_ID;
	private String State_Definition;
	
	public int getJIRA_ID() {
		return JIRA_ID;
	}
	public void setJIRA_ID(int jIRA_ID) {
		JIRA_ID = jIRA_ID;
	}
	public String getJIRA_Desc() {
		return JIRA_Desc;
	}
	public void setJIRA_Desc(String jIRA_Desc) {
		JIRA_Desc = jIRA_Desc;
	}
	public int getRequirement_ID() {
		return Requirement_ID;
	}
	public void setRequirement_ID(int requirement_ID) {
		Requirement_ID = requirement_ID;
	}
	public int getFunctional_Owner_User_ID() {
		return Functional_Owner_User_ID;
	}
	public void setFunctional_Owner_User_ID(int functional_Owner_User_ID) {
		Functional_Owner_User_ID = functional_Owner_User_ID;
	}
	public int getProject_ID() {
		return Project_ID;
	}
	public void setProject_ID(int project_ID) {
		Project_ID = project_ID;
	}
	public int getRelease_ID() {
		return Release_ID;
	}
	public void setRelease_ID(int release_ID) {
		Release_ID = release_ID;
	}
	public Date getStart_Date() {
		return Start_Date;
	}
	public void setStart_Date(Date start_Date) {
		Start_Date = start_Date;
	}
	public Date getEnd_Date() {
		return End_Date;
	}
	public void setEnd_Date(Date end_Date) {
		End_Date = end_Date;
	}
	public Date getState_Modified() {
		return State_Modified;
	}
	public void setState_Modified(Date state_Modified) {
		State_Modified = state_Modified;
	}
	public int getModifiedBy_User_ID() {
		return ModifiedBy_User_ID;
	}
	public void setModifiedBy_User_ID(int modifiedBy_User_ID) {
		ModifiedBy_User_ID = modifiedBy_User_ID;
	}
	public String getState_Definition() {
		return State_Definition;
	}
	public void setState_Definition(String state_Definition) {
		State_Definition = state_Definition;
	}

	@Override
	public String toString() {
		return "JiraStateReturnModel [JIRA_ID=" + JIRA_ID + ", JIRA_Desc=" + JIRA_Desc + ", Requirement_ID="
				+ Requirement_ID + ", Functional_Owner_User_ID=" + Functional_Owner_User_ID + ", Project_ID="
				+ Project_ID + ", Release_ID=" + Release_ID + ", Start_Date=" + Start_Date + ", End_Date=" + End_Date
				+ ", State_Modified=" + State_Modified + ", ModifiedBy_User_ID=" + ModifiedBy_User_ID
				+ ", State_Definition=" + State_Definition + "]";
	}
}
