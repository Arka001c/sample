package com.simpsoft.qadashboard.Models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "state_definition")
public class StateDefinitionModel {
	
	@Id
	private int State_Definition_ID;
	private String State_Definition;
	public int getState_Definition_ID() {
		return State_Definition_ID;
	}
	public void setState_Definition_ID(int state_Definition_ID) {
		State_Definition_ID = state_Definition_ID;
	}
	public String getState_Definition() {
		return State_Definition;
	}
	public void setState_Definition(String state_Definition) {
		State_Definition = state_Definition;
	}
}
