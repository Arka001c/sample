package com.simpsoft.qadashboard.Models;

import java.util.Date;
import java.util.List;
import org.springframework.stereotype.Component;
import com.simpsoft.qadashboard.Models.XML.ExceptionModel;
import com.simpsoft.qadashboard.Models.XML.PropertyModel;

@Component
public class XMLToDBModel {
	
	private Date date;
	private String millis;
	private int sequence;
	private String level;
	private String logger;
	private String classString;
	private String method;
	private int thread;
	private String message;
	private int nestedLevel;
	private ExceptionModel exception;
	private List<PropertyModel> property;
	
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getMillis() {
		return millis;
	}
	public void setMillis(String millis) {
		this.millis = millis;
	}
	public int getSequence() {
		return sequence;
	}
	public void setSequence(int sequence) {
		this.sequence = sequence;
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	public String getLogger() {
		return logger;
	}
	public void setLogger(String logger) {
		this.logger = logger;
	}
	public String getClassString() {
		return classString;
	}
	public void setClassString(String classString) {
		this.classString = classString;
	}
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	public int getThread() {
		return thread;
	}
	public void setThread(int thread) {
		this.thread = thread;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getNestedLevel() {
		return nestedLevel;
	}
	public void setNestedLevel(int nestedLevel) {
		this.nestedLevel = nestedLevel;
	}
	public ExceptionModel getException() {
		return exception;
	}
	public void setException(ExceptionModel exception) {
		this.exception = exception;
	}
	public List<PropertyModel> getProperty() {
		return property;
	}
	public void setProperty(List<PropertyModel> property) {
		this.property = property;
	}
	@Override
	public String toString() {
		return "XMLToDBModel [date=" + date + ", millis=" + millis + ", sequence=" + sequence + ", level=" + level
				+ ", classString=" + classString + ", method=" + method + ", thread=" + thread + ", message=" + message
				+ ", nestedLevel=" + nestedLevel + ", exception=" + exception + ", property=" + property + "]";
	}
}
