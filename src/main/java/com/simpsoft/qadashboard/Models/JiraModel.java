package com.simpsoft.qadashboard.Models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="jira")
public class JiraModel {

	@Id
	private int JIRA_ID;
	private String JIRA_Desc;
	private int Requirement_ID;
	private int Functional_Owner_User_ID;
	public int getJIRA_ID() {
		return JIRA_ID;
	}
	public void setJIRA_ID(int jIRA_ID) {
		JIRA_ID = jIRA_ID;
	}
	public String getJIRA_Desc() {
		return JIRA_Desc;
	}
	public void setJIRA_Desc(String jIRA_Desc) {
		JIRA_Desc = jIRA_Desc;
	}
	public int getRequirement_ID() {
		return Requirement_ID;
	}
	public void setRequirement_ID(int requirement_ID) {
		Requirement_ID = requirement_ID;
	}
	public int getFunctional_Owner_User_ID() {
		return Functional_Owner_User_ID;
	}
	public void setFunctional_Owner_User_ID(int functional_Owner_User_ID) {
		Functional_Owner_User_ID = functional_Owner_User_ID;
	}	
}
