package com.simpsoft.qadashboard.Models;

import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class jiraChartsModel {
	private List<JiraCountByStatusModel> jiraList;
	private int data_type;
	private int month;
	private int week;
	
	public List<JiraCountByStatusModel> getJiraList() {
		return jiraList;
	}
	public void setJiraList(List<JiraCountByStatusModel> jiraList) {
		this.jiraList = jiraList;
	}
	public int getData_type() {
		return data_type;
	}
	public void setData_type(int data_type) {
		this.data_type = data_type;
	}
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		this.month = month;
	}
	public int getWeek() {
		return week;
	}
	public void setWeek(int week) {
		this.week = week;
	}
}
