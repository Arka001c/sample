package com.simpsoft.qadashboard.Models;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="projects")
public class ProjectsModel {

	@Id
	private int Project_ID;
	private String Project_Name;
	private int Active_Flag;
	private Date Update_Date;
	
	public int getProject_ID() {
		return Project_ID;
	}
	public void setProject_ID(int project_ID) {
		Project_ID = project_ID;
	}
	public String getProject_Name() {
		return Project_Name;
	}
	public void setProject_Name(String project_Name) {
		Project_Name = project_Name;
	}
	public int getActive_Flag() {
		return Active_Flag;
	}
	public void setActive_Flag(int active_Flag) {
		Active_Flag = active_Flag;
	}
	public Date getUpdate_Date() {
		return Update_Date;
	}
	public void setUpdate_Date(Date update_Date) {
		Update_Date = update_Date;
	}

}
