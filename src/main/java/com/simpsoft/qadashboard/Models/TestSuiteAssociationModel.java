package com.simpsoft.qadashboard.Models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="test_suite_association")
public class TestSuiteAssociationModel {

	@Id
	private int Test_Suite_Association_ID;
	private int Test_Suite_ID;
	private int Module_ID;
	private int JIRA_ID;
	private int Release_Run_Association_ID;
	
	public int getTest_Suite_Association_ID() {
		return Test_Suite_Association_ID;
	}
	public void setTest_Suite_Association_ID(int test_Suite_Association_ID) {
		Test_Suite_Association_ID = test_Suite_Association_ID;
	}
	public int getTest_Suite_ID() {
		return Test_Suite_ID;
	}
	public void setTest_Suite_ID(int test_Suite_ID) {
		Test_Suite_ID = test_Suite_ID;
	}
	public int getModule_ID() {
		return Module_ID;
	}
	public void setModule_ID(int module_ID) {
		Module_ID = module_ID;
	}
	public int getJIRA_ID() {
		return JIRA_ID;
	}
	public void setJIRA_ID(int jIRA_ID) {
		JIRA_ID = jIRA_ID;
	}
	public int getRelease_Run_Association_ID() {
		return Release_Run_Association_ID;
	}
	public void setRelease_Run_Association_ID(int release_Run_Association_ID) {
		Release_Run_Association_ID = release_Run_Association_ID;
	}
	
}
