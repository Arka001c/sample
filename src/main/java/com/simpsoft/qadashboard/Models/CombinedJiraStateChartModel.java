package com.simpsoft.qadashboard.Models;

import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class CombinedJiraStateChartModel {

	private List<jiraChartsModel> jiraChart;
	private List<JiraStateReturnModel> jiraState;
	private String graph_chart;

	
	public List<jiraChartsModel> getJiraChart() {
		return jiraChart;
	}
	public void setJiraChart(List<jiraChartsModel> jiraChart) {
		this.jiraChart = jiraChart;
	}
	public List<JiraStateReturnModel> getJiraState() {
		return jiraState;
	}
	public void setJiraState(List<JiraStateReturnModel> jiraState) {
		this.jiraState = jiraState;
	}
	public String getGraph_chart() {
		return graph_chart;
	}
	public void setGraph_chart(String graph_chart) {
		this.graph_chart = graph_chart;
	}
}
