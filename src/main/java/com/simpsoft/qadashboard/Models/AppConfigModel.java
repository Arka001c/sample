package com.simpsoft.qadashboard.Models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="app_config")
public class AppConfigModel {

	@Id
	private int App_Config_ID;
	private String Config_Name;
	private String Description;
	private String Config_Value;
	private String Project_Name;
	private String Banner_Logo;
	private String Static_Password;
	private String Footer;
	
	public int getApp_Config_ID() {
		return App_Config_ID;
	}
	public void setApp_Config_ID(int app_Config_ID) {
		App_Config_ID = app_Config_ID;
	}
	public String getConfig_Name() {
		return Config_Name;
	}
	public void setConfig_Name(String config_Name) {
		Config_Name = config_Name;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	public String getConfig_Value() {
		return Config_Value;
	}
	public void setConfig_Value(String config_Value) {
		Config_Value = config_Value;
	}
	public String getProject_Name() {
		return Project_Name;
	}
	public void setProject_Name(String project_Name) {
		Project_Name = project_Name;
	}
	public String getBanner_Logo() {
		return Banner_Logo;
	}
	public void setBanner_Logo(String banner_Logo) {
		Banner_Logo = banner_Logo;
	}
	public String getStatic_Password() {
		return Static_Password;
	}
	public void setStatic_Password(String static_password) {
		Static_Password = static_password;
	}
	public String getFooter() {
		return Footer;
	}
	public void setFooter(String footer) {
		Footer = footer;
	}
	
}
