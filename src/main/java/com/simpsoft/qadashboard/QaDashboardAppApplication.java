package com.simpsoft.qadashboard;
import java.io.FileNotFoundException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class QaDashboardAppApplication {// extends SpringBootServletInitializer {
	
	public static void main(String[] args) throws FileNotFoundException {
		SpringApplication.run(QaDashboardAppApplication.class, args);
	}
	
//	@Override
//    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
//        return builder.sources(QaDashboardAppApplication.class);
//    }
	
}
