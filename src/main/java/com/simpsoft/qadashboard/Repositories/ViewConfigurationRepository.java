package com.simpsoft.qadashboard.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import com.simpsoft.qadashboard.Models.ViewConfigurationModel;

public interface ViewConfigurationRepository extends JpaRepository<ViewConfigurationModel, Integer> {

}
