package com.simpsoft.qadashboard.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import com.simpsoft.qadashboard.Models.RequirementsModel;

public interface RequirementsRepository extends JpaRepository<RequirementsModel, Integer> {

}
