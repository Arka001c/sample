package com.simpsoft.qadashboard.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import com.simpsoft.qadashboard.Models.ModulesModel;

public interface ModulesRepository extends JpaRepository<ModulesModel, Integer> {

}
