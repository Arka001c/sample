package com.simpsoft.qadashboard.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import com.simpsoft.qadashboard.Models.EntityFieldsModel;

public interface EntityFieldsRepository extends JpaRepository<EntityFieldsModel, Integer> {

}
