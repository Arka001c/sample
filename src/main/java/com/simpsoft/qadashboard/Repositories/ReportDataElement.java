package com.simpsoft.qadashboard.Repositories;

import java.util.Map;

import org.springframework.stereotype.Component;

@Component
public interface ReportDataElement {

	public Map<String, String> getData();
}

