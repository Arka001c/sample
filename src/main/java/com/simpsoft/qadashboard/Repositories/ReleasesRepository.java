package com.simpsoft.qadashboard.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import com.simpsoft.qadashboard.Models.ReleasesModel;

public interface ReleasesRepository extends JpaRepository<ReleasesModel, Integer> {

}
