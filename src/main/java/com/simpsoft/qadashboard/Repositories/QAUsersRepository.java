package com.simpsoft.qadashboard.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.simpsoft.qadashboard.Models.QAUsersModel;

public interface QAUsersRepository extends JpaRepository<QAUsersModel, Integer> {

}
