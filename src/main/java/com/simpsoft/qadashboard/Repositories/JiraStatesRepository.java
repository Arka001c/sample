package com.simpsoft.qadashboard.Repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.simpsoft.qadashboard.Models.JiraStateReturnModel;

public interface JiraStatesRepository extends JpaRepository<JiraStateReturnModel, Integer> {

//	@Query(value = "SELECT j.JIRA_ID, r.Start_Date, (CASE WHEN r.End_Date IS NOT NULL THEN r.End_Date WHEN r.End_Date IS NULL THEN DATE(NOW()) END) AS 'End_Date', js.State_Modified, js.modified_by_user_id, sd.State_Definition\r\n"
//			+ "FROM jira j, requirements rq, releases r, jira_states js, state_definition sd\r\n"
//			+ "WHERE j.Requirement_ID=rq.Requirement_ID AND rq.Release_ID=r.Release_ID AND js.JIRA_ID=j.JIRA_ID AND sd.State_Definition_ID=js.JIRA_State", nativeQuery = true)

	@Query(value = "SELECT j.JIRA_ID, j.JIRA_Desc, rq.Requirement_ID, j.Functional_Owner_User_ID, rq.Project_ID, rq.Release_ID, r.Start_Date, (CASE WHEN r.End_Date IS NOT NULL THEN r.End_Date WHEN r.End_Date IS NULL THEN DATE(NOW()) END) AS 'End_Date', js.State_Modified, js.ModifiedBy_User_ID as modified_by_user_id, sd.State_Definition\r\n"
			+ "FROM jira j, requirements rq, releases r, jira_states js, state_definition sd\r\n"
			+ "WHERE j.Requirement_ID=rq.Requirement_ID AND rq.Release_ID=r.Release_ID AND js.JIRA_ID=j.JIRA_ID AND sd.State_Definition_ID=js.JIRA_State", nativeQuery = true)
	List<JiraStateReturnModel> getAllData();
}
