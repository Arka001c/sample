package com.simpsoft.qadashboard.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import com.simpsoft.qadashboard.Models.TestSuiteAssociationModel;

public interface TestSuiteAssociationRepository extends JpaRepository<TestSuiteAssociationModel, Integer> {

}
