package com.simpsoft.qadashboard.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import com.simpsoft.qadashboard.Models.AppConfigModel;

public interface AppConfigRepository extends JpaRepository<AppConfigModel, Integer> {

}
