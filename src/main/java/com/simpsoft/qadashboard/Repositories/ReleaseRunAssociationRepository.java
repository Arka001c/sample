package com.simpsoft.qadashboard.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import com.simpsoft.qadashboard.Models.ReleaseRunAssociationModel;

public interface ReleaseRunAssociationRepository extends JpaRepository<ReleaseRunAssociationModel, Integer> {

}
