package com.simpsoft.qadashboard.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import com.simpsoft.qadashboard.Models.ProjectsModel;

public interface ProjectsRepository extends JpaRepository<ProjectsModel, Integer> {

}
