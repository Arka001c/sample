package com.simpsoft.qadashboard.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.simpsoft.qadashboard.Models.JiraModel;

public interface JiraRepository extends JpaRepository<JiraModel, Integer> {

}
