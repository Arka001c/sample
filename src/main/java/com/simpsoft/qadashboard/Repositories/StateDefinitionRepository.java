package com.simpsoft.qadashboard.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.simpsoft.qadashboard.Models.StateDefinitionModel;

public interface StateDefinitionRepository extends JpaRepository<StateDefinitionModel, Integer>{

}
