package com.simpsoft.qadashboard.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import com.simpsoft.qadashboard.Models.ViewDrillDownModel;

public interface ViewDrillDownRepository extends JpaRepository<ViewDrillDownModel, Integer> {

}
