package com.simpsoft.qadashboard.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import com.simpsoft.qadashboard.Models.ViewConfigurationFiltersModel;

public interface ViewConfigurationFiltersRepository extends JpaRepository<ViewConfigurationFiltersModel, Integer> {

}
