package com.simpsoft.qadashboard;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;
import com.simpsoft.qadashboard.Repositories.ReportDataElement;

@Component
public class DrillDownNode {
	private String nodeType;
	private String nodeName;
	private Map<String, DrillDownNode> elements;
	private List<ReportDataElement> data;

	public DrillDownNode() {
		super();
	}
	
	public DrillDownNode(String elementName, String elementType) {
		elements = new HashMap<String, DrillDownNode>();
		data = new ArrayList<ReportDataElement>();
		nodeName = elementName;
		nodeType = elementType;
	}
	
	public String getNodeType() {
		return nodeType;
	}

	public void setNodeType(String nodeType) {
		this.nodeType = nodeType;
	}

	public String getNodeName() {
		return nodeName;
	}

	public void setNodeName(String nodeName) {
		this.nodeName = nodeName;
	}

	public Map<String, DrillDownNode> getElements() {
		return elements;
	}
	public void setElements(Map<String, DrillDownNode> elements) {
		this.elements = elements;
	}

	public List<ReportDataElement> getData() {
		return data;
	}

	public void setData(List<ReportDataElement> data) {
		this.data = data;
	}

	public void pushNode(String key, String elementType) {
		if (!elements.containsKey(key)) {
			elements.put(key, new DrillDownNode(key, elementType));
		}
	}

	public void pushData(ReportDataElement dataElement, String key) {
		data.add(dataElement);
	}

	public void pushDataToKey(ReportDataElement dataElement, String key) {
		elements.get(key).pushData(dataElement, null);
	}

	public void pushAll(List<ReportDataElement> dataElements) {
		data.addAll(dataElements);
	}

}
