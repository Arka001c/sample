package com.simpsoft.qadashboard;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;
import org.springframework.stereotype.Component;
import com.simpsoft.qadashboard.Repositories.ReportDataElement;

@Component
public class Jiras implements ReportDataElement {

	Map<String, String> data;

	public Jiras() {
		super();
	}
	
	public Jiras(JSONObject sampleData) {
		data = new HashMap<String, String>();
		for (String key : sampleData.keySet()) {
			data.put(key, (sampleData.get(key)).toString());
		}
	}

	@Override
	public Map<String, String> getData() {
		return data;
	}

}
