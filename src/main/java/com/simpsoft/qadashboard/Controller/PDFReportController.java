package com.simpsoft.qadashboard.Controller;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.stream.Stream;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.simpsoft.qadashboard.Models.GetPath;

@RestController
public class PDFReportController {
	
	@Autowired
	GetPath imgFilePath;
	
	
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(path = "/downloadpdf", method = RequestMethod.POST)
	public ResponseEntity<InputStreamResource> pdfdownload(HttpServletRequest request) throws Exception {
		
		String frmJiraList = request.getParameter("frmJiraListPDF");
		String imgFile = request.getParameter("frmJiraListImagePDF");
		imgFile = imgFile.replace("\"", "");
		JSONArray jiraList = new JSONArray(frmJiraList);
		
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		
		Document document = new Document();
		PdfWriter.getInstance(document, out);
		document.open();
		
		
		PdfPTable table = new PdfPTable(7);
		
		Stream.of("Jira ID", "Jira Desc", "Project ID", "Release ID", "Requirement ID", "Resource ID", "Status")
        .forEach(headerTitle -> {
            PdfPCell PDFheader = new PdfPCell();
            
            Font headFont = FontFactory.getFont(FontFactory.TIMES_BOLD, 12, new BaseColor(255, 255, 255));
            
            PDFheader.setBackgroundColor(new BaseColor(0, 51, 102));
            PDFheader.setHorizontalAlignment(Element.ALIGN_CENTER);
            PDFheader.setPhrase(new Phrase(headerTitle, headFont));
            
            table.addCell(PDFheader);
        });
		
		for(int i=0;i<jiraList.length();i++) {
			JSONObject object =jiraList.getJSONObject(i);
			
			table.addCell(object.get("jira_ID").toString());
			table.addCell(object.get("jira_Desc").toString());
			table.addCell(object.get("project_ID").toString());
			table.addCell(object.get("release_ID").toString());
			table.addCell(object.get("requirement_ID").toString());
			table.addCell(object.get("functional_Owner_User_ID").toString());
			table.addCell(object.get("state_Definition").toString());
		}
		
		
		document.add(table);
		
		Image image = Image.getInstance(imgFilePath.getGetPath() + imgFile + ".jpeg");
		image.scaleToFit(400, 300);
		document.add(image);
		
		document.close();
		
		
		HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=jiras.pdf");
        
		return ResponseEntity
                .ok()
                .headers(headers)
                .contentType(MediaType.APPLICATION_PDF)
                .body(new InputStreamResource(new ByteArrayInputStream(out.toByteArray())));
	}
}
