package com.simpsoft.qadashboard.Controller;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.simpsoft.qadashboard.Models.AppConfigModel;
import com.simpsoft.qadashboard.Models.CustomViewConfigModel;
import com.simpsoft.qadashboard.Models.EntityFieldsModel;
import com.simpsoft.qadashboard.Models.GetPath;
import com.simpsoft.qadashboard.Models.QAUsersModel;
import com.simpsoft.qadashboard.Models.ViewConfigurationFiltersModel;
import com.simpsoft.qadashboard.Models.ViewConfigurationModel;
import com.simpsoft.qadashboard.Models.ViewDrillDownModel;
import com.simpsoft.qadashboard.Models.XMLToDBModel;
import com.simpsoft.qadashboard.Models.XML.ExceptionModel;
import com.simpsoft.qadashboard.Models.XML.FrameModel;
import com.simpsoft.qadashboard.Models.XML.PropertyModel;
import com.simpsoft.qadashboard.Repositories.AppConfigRepository;
import com.simpsoft.qadashboard.Repositories.EntityFieldsRepository;
import com.simpsoft.qadashboard.Repositories.QAUsersRepository;
import com.simpsoft.qadashboard.Repositories.ViewConfigurationFiltersRepository;
import com.simpsoft.qadashboard.Repositories.ViewConfigurationRepository;
import com.simpsoft.qadashboard.Repositories.ViewDrillDownRepository;

@RestController
public class MainController {

	@Autowired
	private AppConfigRepository appConfigRepo;
	@Autowired
	private QAUsersRepository qaUsersRepo;
	@Autowired
	private ViewConfigurationRepository viewConfigRepo;
	@Autowired
	private ViewConfigurationFiltersRepository viewConfigFilterRepo;
	@Autowired
	private ViewDrillDownRepository viewDrillDownRepo;
	@Autowired
	private EntityFieldsRepository entityFieldRepo;
	@Autowired
	GetPath getPath;

	// >> Starting of Shahid's
	// Work--------------------------------------------------------------//
	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping("/ListOfEntity")
	public List<EntityFieldsModel> ListOfEntity() {
		return entityFieldRepo.findAll();
	}

	@CrossOrigin(origins = "http://localhost:4200")
	@PostMapping("/configureView")
	public CustomViewConfigModel save(@RequestBody CustomViewConfigModel cust) {

		ViewConfigurationModel vcm = new ViewConfigurationModel();

		vcm.setOwner(cust.getOwner());
		vcm.setView_Configuration_Name(cust.getView_Configuration_Name());
		vcm.setPrivate_Flag(1);
		vcm = viewConfigRepo.saveAndFlush(vcm);

		int config_id = vcm.getView_Configuration_ID();

		ViewConfigurationFiltersModel vcfm = new ViewConfigurationFiltersModel();
		vcfm.setFilter_Field_Value(cust.getFilter_Field_Value());
		vcfm.setFilter_Field_ID(cust.getFilter_Field_ID());
		vcfm.setView_Configuration_ID(config_id);
		vcfm.setFilter_Field(cust.getFilter_Field());
		viewConfigFilterRepo.save(vcfm);

		int count = 1;
		List<ViewDrillDownModel> vddlmList = new ArrayList<ViewDrillDownModel>();
		for (String item : cust.getDrillDownList()) {
			ViewDrillDownModel vddlm = new ViewDrillDownModel();
			vddlm.setDrill_Down_Sl(count);
			vddlm.setView_Configuration_ID(config_id);
			vddlm.setField_ID(Integer.parseInt(item));
			vddlmList.add(vddlm);
			count++;
		}
		viewDrillDownRepo.saveAll(vddlmList);
		return cust;
	}

	@CrossOrigin(origins = "http://localhost:4200")
	@PostMapping("/configureViewUpdate/{id}")
	public CustomViewConfigModel updateConfig(@PathVariable int id, @RequestBody CustomViewConfigModel cust) {

		deletionOfConview(id);
		return save(cust);
	}

	@CrossOrigin(origins = "http://localhost:4200")
	@DeleteMapping("/deletionOfConview/{id}")
	public List<ViewConfigurationModel> deletionOfConview(@PathVariable int id) {
		List<ViewConfigurationModel> vcm = new ArrayList<ViewConfigurationModel>();
		vcm = viewConfigRepo.findAll().stream().filter(item -> item.getView_Configuration_ID() == id)
				.collect(Collectors.toList());
		viewConfigRepo.deleteById(id);
		return vcm;
	}

	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping("/getConfiguration/{id}")
	public CustomViewConfigModel getConfiguration(@PathVariable int id) {

		CustomViewConfigModel data = new CustomViewConfigModel();

		for (ViewConfigurationModel item : viewConfigRepo.findAll().stream()
				.filter(x -> x.getView_Configuration_ID() == id).collect(Collectors.toList())) {
			data.setView_Configuration_ID(item.getView_Configuration_ID());
			data.setView_Configuration_Name(item.getView_Configuration_Name());
		}

		for (ViewConfigurationFiltersModel item : viewConfigFilterRepo.findAll().stream()
				.filter(x -> x.getView_Configuration_ID() == id).collect(Collectors.toList())) {
			data.setFilter_Field(item.getFilter_Field());
			data.setFilter_Field_ID(item.getFilter_Field_ID());
			data.setFilter_Field_Value(item.getFilter_Field_Value());
		}

		List<String> listDrill = new ArrayList<String>();
		for (ViewDrillDownModel item : viewDrillDownRepo.findAll().stream()
				.filter(x -> x.getView_Configuration_ID() == id).collect(Collectors.toList())) {
			listDrill.add(String.valueOf(item.getField_ID()));
		}
		data.setDrillDownList(listDrill);
		return data;
	}
	// >> Ending of Shahid's Work
	// --------------------------------------------------------------//

	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping("/ListUsers")
	public List<QAUsersModel> ListUsers() {
		return qaUsersRepo.findAll();
	}

	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping("/GetAppConfig")
	public List<AppConfigModel> GetAppConfig() {
		return appConfigRepo.findAll();
	}

	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping("/LoadMenuItems/{user_id}")
	public List<ViewConfigurationModel> LoadMenuItems(@PathVariable int user_id) {

		return viewConfigRepo.findAll().stream().filter(item -> item.getOwner() == user_id)
				.collect(Collectors.toList());
	}

	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping("/LoadDataFromMenu/{id}")
	public List<ViewConfigurationFiltersModel> LoadDataFromMenu(@PathVariable int id) {

		return viewConfigFilterRepo.findAll().stream().filter(item -> item.getFilter_Field() == id)
				.collect(Collectors.toList());
	}

	@SuppressWarnings("deprecation")
	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping("/xmltodb")
	public List<XMLToDBModel> xmltodb() throws FileNotFoundException, IOException, XMLStreamException {
//		List<XMLToDBModel>;
		File file = new File(getPath.getXMLPath());
		String xml = inputStreamToString(new FileInputStream(file));

		XMLInputFactory factory = XMLInputFactory.newInstance();
		InputStream stream = new ByteArrayInputStream(xml.getBytes());
		XMLStreamReader reader = factory.createXMLStreamReader(stream);

		StringBuilder content = null;
		
		StringBuilder record = null;
		List<XMLToDBModel> xmlModel = new ArrayList<XMLToDBModel>();
		XMLToDBModel recordModel = null;
		
		List<PropertyModel> xmlPropertyModel = null;
		PropertyModel propertyModel = null;
		ExceptionModel exceptionModel = null;
		StringBuilder frame = null;
		List<FrameModel> xmlFrameModel = null;
		
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(JsonParser.Feature.ALLOW_BACKSLASH_ESCAPING_ANY_CHARACTER, true); 
		
		boolean hasProperty = false;
		boolean hasException = false;
		boolean hasFrame = false;
		while (reader.hasNext()) {

			int event = reader.next();
			switch (event) {
			case XMLStreamConstants.START_ELEMENT:
				reader.getLocalName();
				if (reader.getLocalName().equals("record")) {
					
					if (record != null) {
						if (exceptionModel == null && propertyModel == null) {
							record.setLength(record.length() - 1);
						} else {
							if (propertyModel != null) {
								hasProperty = false;
								record.append("\"property\" : null");
							}
							if (frame != null) {
								frame.append("}]");
								hasFrame = false;
								xmlFrameModel = Arrays.asList(mapper.readValue(frame.toString(), FrameModel[].class));
								exceptionModel.setFrame(xmlFrameModel);
							}
							if (exceptionModel != null) {
								hasException = false;
								record.append("\"exception\" : null");
							}
						}
						
						record.append("}");
						
						recordModel = new XMLToDBModel();
						recordModel = mapper.readValue(record.toString(), XMLToDBModel.class);
						recordModel.setProperty(xmlPropertyModel);
						recordModel.setException(exceptionModel);
						
						xmlModel.add(recordModel);
						
						content = null;
						record = null;
						recordModel = null;
						propertyModel = null;
						exceptionModel = null;
						frame = null;
						xmlFrameModel = null;
					}
					record = new StringBuilder();
					record.append("{");
					
				} else if (reader.getLocalName().equals("property")) {
					if (!hasProperty) {
						hasProperty = true;
						xmlPropertyModel = new ArrayList<PropertyModel>();
					}
					propertyModel = new PropertyModel();
					propertyModel.setName(reader.getAttributeValue(0));
				} else if (reader.getLocalName().equals("exception")) {
					if (!hasException) {
						hasException = true;
						exceptionModel = new ExceptionModel();
					}
				} else if (reader.getLocalName().equals("frame")) {
					if (!hasFrame) {
						hasFrame = true;

						frame = new StringBuilder();
						xmlFrameModel = new ArrayList<FrameModel>();
						frame.append("[{");
					} else if (frame != null) {

						if (frame.substring(frame.length() - 1, frame.length()).equals(",")) {
							frame.setLength(frame.length() - 1);
						}
						frame.append("},{");
					}
				}
				content = new StringBuilder();
				break;

			case XMLStreamConstants.CHARACTERS:
				if (content != null) {
					content.append(reader.getText().trim());
				}
				break;

			case XMLStreamConstants.END_ELEMENT:
				if (content != null) {
					if (reader.getLocalName().equals("property")) {
						propertyModel.setValue(content.toString());
						xmlPropertyModel.add(propertyModel);
					} else {
						if (!hasException && !hasFrame)
							record.append("\"" + reader.getLocalName() + "\"" + " : \"" + content.toString() + "\",");
					}
					if (hasException) {
						if (reader.getLocalName().equals("message")) {
							exceptionModel.setMessage(content.toString());
						}
						if (hasFrame) {
							if (reader.getLocalName().equals("classString")) {
								frame.append(
										"\"" + reader.getLocalName() + "\"" + " : \"" + content.toString() + "\",");
							} else if (reader.getLocalName().equals("method")) {
								frame.append(
										"\"" + reader.getLocalName() + "\"" + " : \"" + content.toString() + "\",");
							} else if (reader.getLocalName().equals("line")) {
								frame.append("\"" + reader.getLocalName() + "\"" + " : \"" + content.toString() + "\"");
							}
						}
					}
				}
				content = null;
				break;

			case XMLStreamConstants.START_DOCUMENT:
				break;
			}

		}
		
		if (record != null) {
			if (exceptionModel == null && propertyModel == null) {
				record.setLength(record.length() - 1);
			} else {
				if (propertyModel != null) {
					hasProperty = false;
					record.append("\"property\" : null");
				}
				if (frame != null) {
					frame.append("}]");
					hasFrame = false;
					xmlFrameModel = Arrays.asList(mapper.readValue(frame.toString(), FrameModel[].class));
					exceptionModel.setFrame(xmlFrameModel);
				}
				if (exceptionModel != null) {
					hasException = false;
					record.append("\"exception\" : null");
				}
			}
			
			record.append("}");
			
			recordModel = new XMLToDBModel();
			recordModel = mapper.readValue(record.toString(), XMLToDBModel.class);
			recordModel.setProperty(xmlPropertyModel);
			recordModel.setException(exceptionModel);
			
			xmlModel.add(recordModel);
			
			content = null;
			record = null;
			recordModel = null;
			propertyModel = null;
			exceptionModel = null;
			frame = null;
			xmlFrameModel = null;
		}

//		System.out.println(xmlModel);
		return xmlModel;
	}

	public String inputStreamToString(InputStream is) throws IOException {
		StringBuilder sb = new StringBuilder();
		String line;
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		while ((line = br.readLine()) != null) {
			sb.append(line);
		}
		br.close();
		return sb.toString();
	}
}
