package com.simpsoft.qadashboard.Controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.simpsoft.qadashboard.Models.jiraCSVModel;

@RestController
public class CsvReportController {
	
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(path = "/downloadcsv", method = RequestMethod.POST)
	public void csvdownload(HttpServletResponse response, HttpServletRequest request) throws Exception {
		
		String frmJiraList = request.getParameter("frmJiraList");
		JSONArray jiraList = new JSONArray(frmJiraList);
		
		List<jiraCSVModel> csvReport = new ArrayList<>();
		
		for(int i=0;i<jiraList.length();i++) {
			
			jiraCSVModel jiraOBJ = new jiraCSVModel();
			JSONObject object =jiraList.getJSONObject(i);
			
			jiraOBJ.setJIRA_ID(Integer.parseInt(object.get("jira_ID").toString()));
			jiraOBJ.setJIRA_Desc(object.get("jira_Desc").toString());
			jiraOBJ.setProject_ID(Integer.parseInt(object.get("project_ID").toString()));
			jiraOBJ.setRelease_ID(Integer.parseInt(object.get("release_ID").toString()));
			jiraOBJ.setRequirement_ID(Integer.parseInt(object.get("requirement_ID").toString()));
			jiraOBJ.setFunctional_Owner_User_ID(Integer.parseInt(object.get("functional_Owner_User_ID").toString()));
			jiraOBJ.setState_Definition(object.get("state_Definition").toString());
		
			csvReport.add(jiraOBJ);
		}
		
		//set file name and content type
        String filename = "jiras.csv";

        response.setContentType("text/csv");
        response.setHeader(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=\"" + filename + "\"");

        //create a csv writer
        StatefulBeanToCsv<jiraCSVModel> writer = new StatefulBeanToCsvBuilder<jiraCSVModel>(response.getWriter())
                .withQuotechar(CSVWriter.NO_QUOTE_CHARACTER)
                .withSeparator(CSVWriter.DEFAULT_SEPARATOR)
                .withOrderedResults(false)
                .build();

        //write all users to csv file
        writer.write(csvReport);
	}
}