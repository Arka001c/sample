package com.simpsoft.qadashboard.Controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import org.jfree.chart.ChartColor;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtils;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.jfree.data.category.DefaultCategoryDataset;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import com.simpsoft.qadashboard.Models.GetPath;
import com.simpsoft.qadashboard.Models.JiraCountByStatusModel;
import com.simpsoft.qadashboard.Models.JiraStateReturnModel;
import com.simpsoft.qadashboard.Models.jiraChartsModel;

@RestController
public class GetReport {

	@Autowired
	GetPath getPath;

	public String[] state_definition = {};

	@SuppressWarnings("deprecation")
	public List<jiraChartsModel> getChart(List<JiraStateReturnModel> jiraList, List<String> state, String data_type,
			String byWeek) throws Exception {

		List<jiraChartsModel> jiraChart = new ArrayList<jiraChartsModel>();
		List<Integer> items = new ArrayList<Integer>();

		if (!byWeek.equals("week")) {
			/* Group data according to project id */
			if (data_type.equals("project_ID")) {

				for (JiraStateReturnModel item : jiraList) {
					if (!items.contains(item.getProject_ID())) {
						items.add(item.getProject_ID());
					}
				}
				for (Integer ids : items) {
					jiraChartsModel child = new jiraChartsModel();

					List<JiraCountByStatusModel> jiraCountList = new ArrayList<JiraCountByStatusModel>();
					List<JiraStateReturnModel> jiraListChild = jiraList.stream().filter(x -> x.getProject_ID() == ids)
							.collect(Collectors.toList());

					for (String stateItem : state) {
						int countJira = jiraListChild.stream().filter(x -> x.getState_Definition().equals(stateItem))
								.collect(Collectors.toList()).size();

						if (countJira > 0) {
							JiraCountByStatusModel addCount = new JiraCountByStatusModel();
							addCount.setJira_count(countJira);
							addCount.setJira_status(stateItem);
							jiraCountList.add(addCount);
						}
					}
					child.setJiraList(jiraCountList);
					child.setData_type(ids);
					jiraChart.add(child);
				}

			} else if (data_type.equals("release_ID")) { /* Group data according to release id */

				for (JiraStateReturnModel item : jiraList) {
					if (!items.contains(item.getRelease_ID())) {
						items.add(item.getRelease_ID());
					}
				}
				for (Integer ids : items) {
					jiraChartsModel child = new jiraChartsModel();

					List<JiraCountByStatusModel> jiraCountList = new ArrayList<JiraCountByStatusModel>();
					List<JiraStateReturnModel> jiraListChild = jiraList.stream().filter(x -> x.getRelease_ID() == ids)
							.collect(Collectors.toList());

					for (String stateItem : state) {
						int countJira = jiraListChild.stream().filter(x -> x.getState_Definition().equals(stateItem))
								.collect(Collectors.toList()).size();

						if (countJira > 0) {
							JiraCountByStatusModel addCount = new JiraCountByStatusModel();
							addCount.setJira_count(countJira);
							addCount.setJira_status(stateItem);
							jiraCountList.add(addCount);
						}
					}
					child.setJiraList(jiraCountList);
					child.setData_type(ids);
					jiraChart.add(child);
				}

			} else if (data_type.equals("JIRA_ID")) { /* Group data according to jira id */

				for (JiraStateReturnModel item : jiraList) {
					if (!items.contains(item.getJIRA_ID())) {
						items.add(item.getJIRA_ID());
					}
				}
				for (Integer ids : items) {
					jiraChartsModel child = new jiraChartsModel();

					List<JiraCountByStatusModel> jiraCountList = new ArrayList<JiraCountByStatusModel>();
					List<JiraStateReturnModel> jiraListChild = jiraList.stream().filter(x -> x.getJIRA_ID() == ids)
							.collect(Collectors.toList());

					for (String stateItem : state) {
						int countJira = jiraListChild.stream().filter(x -> x.getState_Definition().equals(stateItem))
								.collect(Collectors.toList()).size();

						if (countJira > 0) {
							JiraCountByStatusModel addCount = new JiraCountByStatusModel();
							addCount.setJira_count(countJira);
							addCount.setJira_status(stateItem);
							jiraCountList.add(addCount);
						}
					}
					child.setJiraList(jiraCountList);
					child.setData_type(ids);
					jiraChart.add(child);
				}

			} else if (data_type.equals("requirement_ID")) { /* Group data according to requirement id */

				for (JiraStateReturnModel item : jiraList) {
					if (!items.contains(item.getRequirement_ID())) {
						items.add(item.getRequirement_ID());
					}
				}
				for (Integer ids : items) {
					jiraChartsModel child = new jiraChartsModel();

					List<JiraCountByStatusModel> jiraCountList = new ArrayList<JiraCountByStatusModel>();
					List<JiraStateReturnModel> jiraListChild = jiraList.stream()
							.filter(x -> x.getRequirement_ID() == ids).collect(Collectors.toList());

					for (String stateItem : state) {
						int countJira = jiraListChild.stream().filter(x -> x.getState_Definition().equals(stateItem))
								.collect(Collectors.toList()).size();

						if (countJira > 0) {
							JiraCountByStatusModel addCount = new JiraCountByStatusModel();
							addCount.setJira_count(countJira);
							addCount.setJira_status(stateItem);
							jiraCountList.add(addCount);
						}
					}
					child.setJiraList(jiraCountList);
					child.setData_type(ids);
					jiraChart.add(child);
				}

			} else if (data_type.equals("functional_Owner_User_ID")) { /* Group data according to resource id */

				for (JiraStateReturnModel item : jiraList) {
					if (!items.contains(item.getFunctional_Owner_User_ID())) {
						items.add(item.getFunctional_Owner_User_ID());
					}
				}
				for (Integer ids : items) {
					jiraChartsModel child = new jiraChartsModel();

					List<JiraCountByStatusModel> jiraCountList = new ArrayList<JiraCountByStatusModel>();
					List<JiraStateReturnModel> jiraListChild = jiraList.stream()
							.filter(x -> x.getFunctional_Owner_User_ID() == ids).collect(Collectors.toList());

					for (String stateItem : state) {
						int countJira = jiraListChild.stream().filter(x -> x.getState_Definition().equals(stateItem))
								.collect(Collectors.toList()).size();

						if (countJira > 0) {
							JiraCountByStatusModel addCount = new JiraCountByStatusModel();
							addCount.setJira_count(countJira);
							addCount.setJira_status(stateItem);
							jiraCountList.add(addCount);
						}
					}
					child.setJiraList(jiraCountList);
					child.setData_type(ids);
					jiraChart.add(child);
				}
			}
		} else {

			List<Integer> months = new ArrayList<Integer>();
			for (JiraStateReturnModel item : jiraList) {
				if (!months.contains(item.getState_Modified().getMonth())) {
					months.add(item.getState_Modified().getMonth());
				}
			}
			Collections.sort(months);

			for (int i = 0; i < months.size(); i++) {
				// >> Get weeks for a particular month
				int month = months.get(i);
				List<Integer> weeks = new ArrayList<Integer>();
				for (JiraStateReturnModel item : jiraList) {

					if (item.getState_Modified().getMonth() == month) {
						
						int weekNumber = (int) Math.ceil(item.getState_Modified().getDate() / 7.0);
						if (!weeks.contains(weekNumber)) {
							weeks.add(weekNumber);
							
							// >> Generate Data for Chart
							jiraChartsModel child = new jiraChartsModel();

							List<JiraCountByStatusModel> jiraCountList = new ArrayList<JiraCountByStatusModel>();
							List<JiraStateReturnModel> jiraListChild = jiraList.stream()
									.filter(x -> x.getState_Modified().getMonth() == month && Math.ceil(item.getState_Modified().getDate() / 7.0) == weekNumber)
									.collect(Collectors.toList());

							for (String stateItem : state) {
								int countJira = jiraListChild.stream()
										.filter(x -> x.getState_Definition().equals(stateItem))
										.collect(Collectors.toList()).size();

								if (countJira > 0) {
									JiraCountByStatusModel addCount = new JiraCountByStatusModel();
									addCount.setJira_count(countJira);
									addCount.setJira_status(stateItem);
									jiraCountList.add(addCount);
								}
							}
							child.setJiraList(jiraCountList);
							child.setData_type(weekNumber);
							child.setWeek(weekNumber);
							child.setMonth(month);
							jiraChart.add(child);
						}
					}
				}
				
			}
		}
		return jiraChart;
	}

	public String GenerateGraphChart(List<jiraChartsModel> jiraChart) throws IOException {
		final DefaultCategoryDataset dataset = new DefaultCategoryDataset();

		for (int i = 0; i < jiraChart.size(); i++) {
			for (int j = 0; j < jiraChart.get(i).getJiraList().size(); j++) {
				if (jiraChart.get(i).getJiraList().get(j).getJira_status().equals("Open\r\n")) {
					dataset.addValue(jiraChart.get(i).getJiraList().get(j).getJira_count(), "Open",
							Integer.toString(jiraChart.get(i).getData_type()));
				}
				if (jiraChart.get(i).getJiraList().get(j).getJira_status().equals("Fixed\r\n")) {
					dataset.addValue(jiraChart.get(i).getJiraList().get(j).getJira_count(), "Fixed",
							Integer.toString(jiraChart.get(i).getData_type()));
				}
				if (jiraChart.get(i).getJiraList().get(j).getJira_status().equals("In-test\r\n")) {
					dataset.addValue(jiraChart.get(i).getJiraList().get(j).getJira_count(), "In-test",
							Integer.toString(jiraChart.get(i).getData_type()));
				}
				if (jiraChart.get(i).getJiraList().get(j).getJira_status().equals("Closed\r\n")) {
					dataset.addValue(jiraChart.get(i).getJiraList().get(j).getJira_count(), "Closed",
							Integer.toString(jiraChart.get(i).getData_type()));
				}
			}
		}

		/* Create Chart */
		JFreeChart barChart = ChartFactory.createStackedBarChart("JIRA Status by Module", "", "", dataset,
				PlotOrientation.VERTICAL, true, true, false);

		/* Get instance of CategoryPlot */
		CategoryPlot plot = barChart.getCategoryPlot();

		/* Changing the background color of the Chart */
		plot.setBackgroundPaint(ChartColor.white);

		/* Change Bar colors */
		BarRenderer renderer = (BarRenderer) plot.getRenderer();

		/* Changing the Chart color theme */
		renderer.setBarPainter(new StandardBarPainter());

		/* setting colors for stacks */
		renderer.setSeriesPaint(0, new ChartColor(102, 102, 255));
		renderer.setSeriesPaint(1, new ChartColor(255, 133, 51));
		renderer.setSeriesPaint(2, new ChartColor(140, 140, 140));
		renderer.setSeriesPaint(3, new ChartColor(255, 219, 77));
		renderer.setDrawBarOutline(false);
		renderer.setItemMargin(0);

		/* setting label for all stacked charts */
		for (int i = 0; i < 4; i++) {
			renderer.setSeriesItemLabelGenerator(i, new StandardCategoryItemLabelGenerator());
			renderer.setSeriesItemLabelsVisible(i, true);
		}
		renderer.setDefaultItemLabelsVisible(true);
		renderer.setDefaultSeriesVisible(true);
		barChart.getCategoryPlot().setRenderer(renderer);

		SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmmss");

		String filename = "chart_" + dateFormat.format(new java.util.Date());
		String ext = ".jpeg";

		int width = 640; /* Width of the image */
		int height = 480; /* Height of the image */
		String path = getPath.getGetPath();
		File BarChart = new File(path + filename + ext);
		ChartUtils.saveChartAsJPEG(BarChart, barChart, width, height);

		return filename;
	}

	@Autowired
	ResourceLoader resourceLoader;

	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping("/getChartName/{file_name}")
	public ResponseEntity<InputStreamResource> getChartName(@PathVariable String file_name)
			throws IOException, Exception {
		File file = new File(getPath.getGetPath() + file_name + ".jpeg");
		System.out.println("File Found : " + file.exists());

		if (file.exists()) {
			return ResponseEntity.ok().contentType(MediaType.IMAGE_JPEG)
					.body(new InputStreamResource(new FileInputStream(file)));
		} else {
			return ResponseEntity.ok().contentType(MediaType.IMAGE_JPEG)
					.body(new InputStreamResource(new FileInputStream(file)));
		}
	}
}
