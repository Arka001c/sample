package com.simpsoft.qadashboard.Controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.simpsoft.qadashboard.DrillDownNode;
import com.simpsoft.qadashboard.Jiras;
import com.simpsoft.qadashboard.Models.CombinedJiraStateChartModel;
import com.simpsoft.qadashboard.Models.CustomFilterReturnModel;
import com.simpsoft.qadashboard.Models.JiraModel;
import com.simpsoft.qadashboard.Models.JiraStateReturnModel;
import com.simpsoft.qadashboard.Models.ProjectsModel;
import com.simpsoft.qadashboard.Models.QAUsersModel;
import com.simpsoft.qadashboard.Models.ReleasesModel;
import com.simpsoft.qadashboard.Models.RequirementsModel;
import com.simpsoft.qadashboard.Models.StateDefinitionModel;
import com.simpsoft.qadashboard.Models.ViewConfigurationFiltersModel;
import com.simpsoft.qadashboard.Models.ViewDrillDownModel;
import com.simpsoft.qadashboard.Models.jiraChartsModel;
import com.simpsoft.qadashboard.Repositories.JiraRepository;
import com.simpsoft.qadashboard.Repositories.JiraStatesRepository;
import com.simpsoft.qadashboard.Repositories.ProjectsRepository;
import com.simpsoft.qadashboard.Repositories.QAUsersRepository;
import com.simpsoft.qadashboard.Repositories.ReleasesRepository;
import com.simpsoft.qadashboard.Repositories.ReportDataElement;
import com.simpsoft.qadashboard.Repositories.RequirementsRepository;
import com.simpsoft.qadashboard.Repositories.StateDefinitionRepository;
import com.simpsoft.qadashboard.Repositories.ViewConfigurationFiltersRepository;
import com.simpsoft.qadashboard.Repositories.ViewDrillDownRepository;

@RestController
public class EntityFieldsController {

	@Autowired
	private ViewDrillDownRepository viewDrillRepo;
	@Autowired
	private ViewConfigurationFiltersRepository viewConfigFilterRepo;

	@Autowired
	private ProjectsRepository projectRepo;
	@Autowired
	private ReleasesRepository releaseRepo;
	@Autowired
	private JiraRepository jiraRepo;
	@Autowired
	private RequirementsRepository requirementRepo;
	@Autowired
	private QAUsersRepository userRepo;
	@Autowired
	private JiraStatesRepository jiraStateRepo;

	@Autowired
	private StateDefinitionRepository stateRepo;
	@Autowired
	private GetReport report;

	List<ProjectsModel> projectData;
	List<ReleasesModel> releaseData;
	List<JiraModel> jiraData;
	List<RequirementsModel> requirementData;
	List<QAUsersModel> userData;

	public String[] drillDown;
	public String[] drillDownList = new String[] { "", "project_ID", "release_ID", "JIRA_ID", "requirement_ID",
			"functional_Owner_User_ID" };
	
	List<CustomFilterReturnModel> returnEntities;
	List<ReportDataElement> jiraList;
	List<JiraStateReturnModel> allJiras;

	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping("/LoadSidebarItems/{view_config_id}")
	public List<CustomFilterReturnModel> LoadSidebarItems(@PathVariable String view_config_id)
			throws FileNotFoundException {
		JSONObject sampleData = new JSONObject();// readFileToJSON("D:\\Simpsoft\\SpringApps\\jiralist.json");
		LoadDrillList(view_config_id);

//		allJiras = getJiraRepo.getAllData();
		allJiras = jiraStateRepo.getAllData();
		
		sampleData.put("jiralist", allJiras);

		jiraList = setupSampleData(sampleData);

		DrillDownNode rootDrillDown = new DrillDownNode("root", "root");
		rootDrillDown.pushAll(jiraList);
		process(0, "", rootDrillDown);

		returnEntities = new ArrayList<CustomFilterReturnModel>();
		display(rootDrillDown, "  ");

		Collections.reverse(returnEntities);
		return returnEntities;
	}

	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping("/LoadDrillList/{view_config_id}")
	private List<Integer> LoadDrillList(@PathVariable String view_config_id) {
		List<ViewDrillDownModel> drillData = viewDrillRepo.findAll().stream()
				.filter(item -> item.getView_Configuration_ID() == Integer.parseInt(view_config_id))
				.collect(Collectors.toList());

		List<Integer> drillList = new ArrayList<Integer>();
		int count = 0;
		drillDown = new String[drillData.size()];
		for (ViewDrillDownModel item : drillData) {

			drillDown[count] = drillDownList[item.getField_ID()];
			drillList.add(item.getField_ID());

			if (item.getField_ID() == 1) {
				projectData = projectRepo.findAll();
			} else if (item.getField_ID() == 2) {
				releaseData = releaseRepo.findAll();
			} else if (item.getField_ID() == 3) {
				jiraData = jiraRepo.findAll();
			} else if (item.getField_ID() == 4) {
				requirementData = requirementRepo.findAll();
			} else if (item.getField_ID() == 5) {
				userData = userRepo.findAll();
			}
			count++;
		}
		return drillList;
	}

	private void display(DrillDownNode rootDrillDown, String indent) {

//		System.out.println(indent + rootDrillDown.getNodeName() + " -> " + rootDrillDown);

		CustomFilterReturnModel item = new CustomFilterReturnModel();
		item.setData_id(rootDrillDown.getNodeName());

		for (int i = 0; i < drillDownList.length; i++) {
			if (drillDownList[i].equals(rootDrillDown.getNodeType())) {
				item.setData_type(String.valueOf(i));
			}
		}

		if (rootDrillDown.getNodeType() == drillDownList[1]) {
			if (projectData != null) {
				item.setData_Name(indent + projectData.stream()
						.filter(child -> child.getProject_ID() == Integer.parseInt(rootDrillDown.getNodeName()))
						.collect(Collectors.toList()).get(0).getProject_Name());
			}
		} else if (rootDrillDown.getNodeType() == drillDownList[2]) {
			if (releaseData != null) {
				item.setData_Name(indent + releaseData.stream()
						.filter(child -> child.getRelease_ID() == Integer.parseInt(rootDrillDown.getNodeName()))
						.collect(Collectors.toList()).get(0).getRelease_Name());
			}
		} else if (rootDrillDown.getNodeType() == drillDownList[3]) {
			if (jiraData != null) {
				item.setData_Name(indent + jiraData.stream()
						.filter(child -> child.getJIRA_ID() == Integer.parseInt(rootDrillDown.getNodeName()))
						.collect(Collectors.toList()).get(0).getJIRA_Desc());
			}
		} else if (rootDrillDown.getNodeType() == drillDownList[4]) {
			if (requirementData != null) {
				item.setData_Name(indent + rootDrillDown.getNodeName());
			}
		} else if (rootDrillDown.getNodeType() == drillDownList[5]) {
			if (userData != null) {
				item.setData_Name(indent + userData.stream()
						.filter(child -> child.getUser_ID() == Integer.parseInt(rootDrillDown.getNodeName()))
						.collect(Collectors.toList()).get(0).getUser_Name());
			}
		}

		for (DrillDownNode drillDownNode : rootDrillDown.getElements().values()) {
			display(drillDownNode, "" + indent);
		}

		returnEntities.add(item);
	}

	private void process(int srl, String filter, DrillDownNode parent) {
		if (srl >= drillDown.length) {
			return;
		}
		for (ReportDataElement jira : parent.getData()) {
			String key = jira.getData().get(drillDown[srl]);
			parent.pushNode(key, drillDown[srl]);
			parent.pushDataToKey(jira, key);
		}
		for (String key : parent.getElements().keySet()) {
			process(srl + 1, key, parent.getElements().get(key));
		}
		return;
	}

	public JSONObject readFileToJSON(String filename) throws FileNotFoundException {
		File datafile = new File(filename);
		InputStream dataSource = new FileInputStream(datafile);
		JSONObject fromJson = (JSONObject) new JSONTokener(dataSource).nextValue();
		return fromJson;
	}

	private static List<ReportDataElement> setupSampleData(JSONObject sampleData) {
		JSONArray sampleDataArray = (JSONArray) sampleData.get("jiralist");
		List<ReportDataElement> sampleDataList = new ArrayList<>();
		sampleDataArray.forEach(sampleDataElement -> {
			sampleDataList.add(new Jiras((JSONObject) sampleDataElement));
		});
		return sampleDataList;
	}

	@CrossOrigin(origins = "http://localhost:4200")
	@PostMapping("/LoadDependentData/{data_id}/{data_type}/{byWeek}")
	public CombinedJiraStateChartModel LoadDependentData(@PathVariable String data_id, @PathVariable String data_type, @PathVariable String byWeek, 
			@RequestBody String idTypeListString) throws Exception {

		int count = 0;
		CombinedJiraStateChartModel combinedModel = new CombinedJiraStateChartModel();
		List<JiraStateReturnModel> sublist = allJiras;

		if (data_type.equals("0")) {
			
			ViewConfigurationFiltersModel vcfm = viewConfigFilterRepo.findAll().stream()
					.filter(x -> x.getView_Configuration_ID() == Integer.parseInt(data_id)).collect(Collectors.toList())
					.get(0);
			
			data_type = String.valueOf(vcfm.getFilter_Field());

		} else {
			ObjectMapper mapper = new ObjectMapper();
			List<CustomFilterReturnModel> idTypeList = Arrays
					.asList(mapper.readValue(idTypeListString, CustomFilterReturnModel[].class));

			Collections.reverse(idTypeList);

			List<JiraStateReturnModel> localList = new ArrayList<JiraStateReturnModel>();
			for (CustomFilterReturnModel item : idTypeList) {

				if (drillDownList[Integer.parseInt(item.getData_type())] == "project_ID") {
					localList = sublist.stream().filter(x -> x.getProject_ID() == Integer.parseInt(item.getData_id()))
							.collect(Collectors.toList());
				}
				if (drillDownList[Integer.parseInt(item.getData_type())] == "release_ID") {
					localList = sublist.stream().filter(x -> x.getRelease_ID() == Integer.parseInt(item.getData_id()))
							.collect(Collectors.toList());
				}
				if (drillDownList[Integer.parseInt(item.getData_type())] == "JIRA_ID") {
					localList = sublist.stream().filter(x -> x.getJIRA_ID() == Integer.parseInt(item.getData_id()))
							.collect(Collectors.toList());
				}
				if (drillDownList[Integer.parseInt(item.getData_type())] == "requirement_ID") {
					localList = sublist.stream()
							.filter(x -> x.getRequirement_ID() == Integer.parseInt(item.getData_id()))
							.collect(Collectors.toList());
				}
				if (drillDownList[Integer.parseInt(item.getData_type())] == "functional_Owner_User_ID") {
					localList = sublist.stream()
							.filter(x -> x.getFunctional_Owner_User_ID() == Integer.parseInt(item.getData_id()))
							.collect(Collectors.toList());
				}

				if (localList.size() > 0) {
					sublist = localList;
				}
			}
		}

		JSONObject sampleData = new JSONObject();
		sampleData.put("jiralist", sublist);

		List<ReportDataElement> subJiraList = setupSampleData(sampleData);

		DrillDownNode rootDrillDown = new DrillDownNode("root", "root");
		rootDrillDown.pushAll(subJiraList);
		process(count, "", rootDrillDown);
		display(rootDrillDown, "   ");

		if (sublist.size() > 0) {
			List<String> stateDefinition = new ArrayList<String>();
			for (StateDefinitionModel item : stateRepo.findAll()) {
				stateDefinition.add(item.getState_Definition());
			}

			List<jiraChartsModel> jiraChart = report.getChart(sublist, stateDefinition, drillDownList[Integer.parseInt(data_type)], byWeek);
			
			combinedModel.setJiraChart(jiraChart);
			combinedModel.setJiraState(sublist);
			combinedModel.setGraph_chart(report.GenerateGraphChart(jiraChart));
			return combinedModel;
		}
		return null;
	}

	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping("/LoadFilterData/{id}")
	public List<CustomFilterReturnModel> LoadFilterData(@PathVariable int id) {
		List<CustomFilterReturnModel> data = new ArrayList<CustomFilterReturnModel>();

		if (id == 1) {
			for (ProjectsModel child : projectRepo.findAll()) {
				CustomFilterReturnModel getData = new CustomFilterReturnModel();

				getData.setData_id(String.valueOf(child.getProject_ID()));
				getData.setData_Name(child.getProject_Name());

				data.add(getData);
			}
		} else if (id == 2) {
			for (ReleasesModel child : releaseRepo.findAll()) {
				CustomFilterReturnModel getData = new CustomFilterReturnModel();

				getData.setData_id(String.valueOf(child.getRelease_ID()));
				getData.setData_Name(child.getRelease_Name());

				data.add(getData);
			}
		} else if (id == 3) {
			for (JiraModel child : jiraRepo.findAll()) {
				CustomFilterReturnModel getData = new CustomFilterReturnModel();

				getData.setData_id(String.valueOf(child.getJIRA_ID()));
				getData.setData_Name(child.getJIRA_Desc());

				data.add(getData);
			}
		} else if (id == 4) {
			for (RequirementsModel child : requirementRepo.findAll()) {
				CustomFilterReturnModel getData = new CustomFilterReturnModel();

				getData.setData_id(String.valueOf(child.getRequirement_ID()));
				getData.setData_Name(child.getRequirement_Desc());

				data.add(getData);
			}
		} else if (id == 5) {
			for (QAUsersModel child : userRepo.findAll()) {
				CustomFilterReturnModel getData = new CustomFilterReturnModel();

				getData.setData_id(String.valueOf(child.getUser_ID()));
				getData.setData_Name(child.getUser_Name());

				data.add(getData);
			}
		}

		return data;
	}

	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping("/UpdateJiras")
	public String UpdateJiras(@RequestBody List<JiraStateReturnModel> jiraState) {
		
		
		return "";
	}
}
